"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class game extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      game.belongsTo(models.pengguna, { foreignKey: "userID" });
    }
  }
  game.init(
    {
      status: DataTypes.STRING,
      waktu: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "game",
    }
  );
  return game;
};
