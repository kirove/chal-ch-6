const express = require("express");
const userRouter = express.Router();
const userController = require("./controller");

userRouter.get("/users", userController.getAllUser);

userRouter.post("/registration", userController.registeredUser);

userRouter.post("/addbio", userController.registeredUserBio);

userRouter.post("/login", userController.userLogin);

userRouter.get("/detail/:idUser", userController.getSingleUser);

// userRouter.put("/detail/:userID", userController.updateSingle);

userRouter.get("/detailbio/:idUser", userController.getSingleUserBio);

userRouter.put("/detailbio/:idUser", userController.updateSingleBio);

userRouter.put("/addhistori", userController.gameHistory);

userRouter.get("/addhistori/:idUser", userController.getSinglegameHistory);

module.exports = userRouter;
